@extends('frontend.layouts.inner-page.master')
@section('content')
    <section id="y-single_info">
        <div class="y-single_info">
            <div class="container">
                <div class="row y-single_info_inner y-section_content">
                    <div class="clearfix">
                        <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s">
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                                <h1 class="y-heading">{{ $product->title }}</h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                <ul class="pull-right">
                                    <li><a href="{{ '/' }}">Home</a></li>
                                    <li><span>Show Product</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
                            <img src="{{ $product->image_url }}" class="img-responsive" alt="">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 pull-right y-product_text_details">
                            <h5>Description</h5>
                            <p>{{ $product->description }}</p>
                            <div class="y-price_box">
                                <span>{{ $product->price }}</span>
                            </div>
                        </div>
                    </div>
                    <div id="y-popular_items" class="y-relative_outer">
                        <div class="container">
                            <h3 class="y-inner_sub_head">Relative Products</h3>
                            <div class="y-popular_items y-relative_item clearfix" id="y-rel_item_slide">
                                @foreach($products as $product)
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="y-relative_img"><img src="{{ $product->image_url}}" class="img-responsive" alt=""></div>
                                        <div class="y-adv_info">
                                            <a class="y-heading" href="{{route('products.show', $product->id)}}">{{ $product->title }}</a>
                                            <div class="y-adv_info_foot clearfix">
                                                <span class="pull-left">$ {{ $product->price }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="y-arrow_line">
                                <i class="fa fa-anchor y-left_anchor"></i>
                                <i class="fa fa-anchor y-right_anchor"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
