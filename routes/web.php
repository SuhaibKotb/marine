<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')
    ->prefix('admin')
    ->name('admin.')
    ->namespace('App\Http\Controllers\Admin')
    ->group(function (){
        Route::get('/', function () {return view('admin.layouts.dashboard');})->name('dashboard');

        Route::resource('products', 'ProductController');
        Route::resource('categories', 'CategoryController');
        Route::resource('users', 'UserController')->except(['show']);
        Route::resource('messages', 'MessageController')->except(['edit', 'update', 'store']);
    });

Route::prefix('/')
    ->namespace('App\Http\Controllers\Frontend')
    ->group(function (){
        Route::get('/', 'HomeController@home')->name('home');
        Route::resource('products', 'ProductController')->only(['index', 'show']);
        Route::resource('categories', 'CategoryController')->only(['index', 'show']);
        Route::get('about', 'HomeController@about')->name('about');
        Route::get('contact', 'HomeController@contact')->name('contact');
        Route::post('contact', 'HomeController@storeMessage')->name('storeMessage');
    });


require __DIR__.'/auth.php';
