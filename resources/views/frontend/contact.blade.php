@extends('frontend.layouts.inner-page.master')
@section('content')
    <div class=" y-contact_02">
        <section id="y-single_info">
      <div class="y-single_info">
        <div class="container">
            <div class="row y-single_info_inner y-section_content">
                <div class="col-lg-12 clearfix">
                  <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s">
                     <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                        <h1 class="y-heading">Contact Us</h1>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                       <ul class="pull-right">
                         <li><a href="{{ route('home') }}">Home</a></li>
                         <li><span>Contact Us</span></li>
                       </ul>
                     </div>
                  </div>
                </div>
                <div class="col-sm-12 clearfix">
                    <div class="y-contact clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 y-contact_side">
                          <div class="row clearfix">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="y-contact_inner">
                                    <h3><i class="material-icons">room</i> Address I</h3>
                                    <address>Egypt, Ismailiyah, Ismailiyah the first , next to Gold's Gym</address>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="y-contact_inner">
                                    <h3><i class="material-icons">room</i> Address II</h3>
                                    <address>8 Gawhar El Qaed St., - Port Tawfik - Suez Next to: In Front Of Kingdom Of Saudi Arabia Consulate</address>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="y-contact_inner">
                                    <h3><i class="material-icons">room</i> Address III</h3>
                                    <address>El Gomhoureya St., Intersection Of De Lesseps St., El Shark District - Port Said – Egypt</address>


                                </div>
                            </div>
                              <br>
                            <div class="col-sm-12 clearfix">
                                  <address>
                                    <span title="Phone">Phone:</span> <a href="tel:+20643890608">+20643890608</a>
                                    <br>
                                    <span title="Phone">Phone:</span> <a href="tel:+201008399497">+201008399497</a>
                                    <br>
                                      <span title="Email">Email:</span> <a href="mailto:ceo@sueztechnomarine.com">ceo@sueztechnomarine.com </a>
                                    <br>
                                    <span title="Email">Email:</span> <a href="mailto:Financial.manager@sueztechnomarine.com">Financial.manager@sueztechnomarine.com</a>
                                      <br>
                                    <span title="Email">Email:</span> <a href="mailto:Sales.advisor@sueztechnomarine.com">Sales.advisor@sueztechnomarine.com</a>
                                      <br>
                                      <br>
                                      <h3>Need help? chat with us <a href="tel:+201008399497">+201008399497</a></h3>
                                  </address>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <div class="y-contact_form">
                                        <form class="y-form" method="post" action="{{ route('storeMessage') }}">
                                            @csrf
                                            <label for="title">
                                                Title
                                            </label>
                                            <input type="text"
                                               id="name"
                                               name="name"
                                               placeholder="Name"
                                               value="{{ old('name') }}"
                                               required
                                               >
                                            <input type="email"
                                               id="email"
                                               name="email"
                                               placeholder="Email"
                                               value="{{ old('email') }}"
                                               required
                                               >
                                            <input type="text"
                                               id="subject"
                                               name="subject"
                                               placeholder="Subject"
                                               value="{{ old('subject') }}"
                                               required
                                            >
                                            <textarea type="text"
                                               id="message"
                                               name="message"
                                               placeholder="Message"
                                               required
                                            >{{ old('message') }}</textarea>
                                            <button class="y-button">Submit</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="y-social">
                                        <a href="#" class="y-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                        <a href="#" class="y-tw"><i class="fa fa-twitter"></i> Twitter</a>
                                        <a href="#" class="y-gp"><i class="fa fa-google-plus"></i> Google+</a>
                                        <a href="#" class="y-yt"><i class="fa fa-youtube"></i> Youtube</a>
                                        <a href="#" class="y-linkd"><i class="fa fa-linkedin"></i> Linkedin</a>
                                        <a href="#" class="y-insta"><i class="fa fa-instagram"></i> Instagram</a>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
    </div>

@endsection
