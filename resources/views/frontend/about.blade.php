@extends('frontend.layouts.inner-page.master')
@section('content')
<div>
    <section id="y-single_info">
          <div class="y-single_info">
            <div class="container">
                <div class="row y-single_info_inner y-section_content">
                    <div class="col-lg-12 clearfix">
                      <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s">
                         <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                            <h1 class="y-heading">About us</h1>
                         </div>
                         <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                           <ul class="pull-right">
                             <li><a href="{{ route('home') }}">Home</a></li>
                             <li><span>About us</span></li>
                           </ul>
                         </div>
                      </div>
                    </div>
                    <div class="col-sm-12 clearfix">
                        <div class="y-corporate_block clearfix">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                              <img src="assets/images/aboutus_main_img.jpg" class="img-responsive" alt="">
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p>
                                Suez Techno-Marine Services L.L.C is established to supply the highest quality ships’ supplies, technical equipment, spare parts and specialist-support service to a worldwide clientele base.
                                In-house experience and practical knowledge go hand-in-hand with the dynamic enthusiasm and aspirations of Suez Techno-Marine Services L.L.C’s forward-looking team.
                                Based on customers’ requirements, Suez Techno-Marine Services L.L.C provides quality equipment and spare parts from the world’s most reliable manufacturers, specializing in a wide scope of supply including Engine Room, Bridge, Galley, Hydraulic and Deck Equipment. Locally, we also offer provisions, stores, and vessel hardening. However, our main activities are based on marine engine repairs, providing: new and reconditioned marine engines, OEM (Original Equipment Manufacturer) or replacement spare parts.
                                Our aim is to provide the highest possible service that our customers deserve. We take pride in our commitment to respond quickly and competitively, to meet the needs of our clientele. Reliability, flexibility, professionalism and the personal touch comprise the foundation of the services and products we provide.
                            </p>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </section>
</div>
@endsection
