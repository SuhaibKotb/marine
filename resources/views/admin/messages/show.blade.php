@extends('admin.layouts.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> Message Details </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.messages.index') }}">Messages</a></li>
              <li class="breadcrumb-item active">{{ Str::limit($message->name, $limit = 15, $end = '...') }}</li>
            </ol>
          </div>
        </div>
      </div>
        <!-- /.container-fluid -->
    </section>

    <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">Read Messages</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div class="mailbox-read-info">
                <h4>Name: {{ $message->name }}</h4>
                <h5>Subject: {{ $message->subject }}</h5>
                  <br>
                <h6>From: {{ $message->email }}</h6>
                <h6>Date: {{ $message->created_at }}</h6>
              </div>

              <div class="mailbox-read-message">
                <h5>Message:</h5>
                <p> {{ $message->message }}</p>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
