<div class="y-inner_header y-sale">
    <div class="y-header_outer">
        <div class="header">
            <div class="container">
                <div class="row clearfix">
                    <div data-wow-duration="1s" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wow fadeInDown text-center">  <a class="y-logo" href="/"><img alt="" src="assets/images/logo1.png"></a>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="y-menu_outer">
                            <div class="rmm style">
                                <ul>
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li><a href="{{ route('products.index') }}">Products</a></li>
                                    <li><a href="{{ route('about') }}">About</a></li>
                                    <li><a href="{{ route('contact') }}">Contact</a></li>
                                </ul>
                            </div>
                            <div class="y-social">
                                <a class="y-fb fa fa-facebook" href="#"></a>
                                <a class="y-tw fa fa-twitter" href="#"></a>
                                <a class="y-gp fa fa-google-plus" href="#"></a>
                                <a class="y-yt fa fa-youtube" href="#"></a>
                                <a class="y-linkd fa fa-linkedin" href="#"></a>
                                <a class="y-insta fa fa-instagram" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
