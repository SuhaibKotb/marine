@extends('frontend.layouts.inner-page.master')
@section('content')
    <section id="y-single_info">
        <div class="y-single_info">
            <div class="container">
                <div class="row y-single_info_inner y-section_content">
                    <div class="col-lg-12 clearfix">
                        <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s">
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                                <h1 class="y-heading">Products Listing</h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                <ul class="pull-right">
                                    <li><a href="{{ '/' }}">Home</a></li>
                                    <li><span>Products Listing</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-3 pull-right">
                            <div class="y-sidebar">
                                <div class="y-product_filters">
                                    <div class="y-single_filter">
                                        <h3>Categories</h3>
                                        <ul>
                                            @foreach($categories as $category)
                                            <li><span><a href="{{ route('products.index', 'category=' . $category->id)}}">{{ $category->name }}</a></span></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="y-product_listing y-product_listing_side clearfix">
                                @forelse($products as $product)
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="y-yacht_intro_img">
                                            <a href="/products/{{ $product->id }}"><img src="{{ $product->image_url }}" class="img-responsive" alt=""></a>
                                        </div>
                                        <div class="y-yacht_intro">
                                            <div style="height: 25px">
                                                <a href="/products/{{ $product->id }}">{{ Str::limit($product->title, 25, '..') }}</a>
                                            </div>
                                            <br>
                                            <span><br>Price :<strong>{{ $product->price }}</strong></span>
                                            <a class="y-button" href="/products/{{ $product->id }}">Show Product</a>
                                        </div>
                                    </div>
                                @empty
                                    <h2>No Products Found</h2>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="y-pagination row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            {{ $products->links('vendor.pagination.frontend') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
