<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <!-- PAGE TITLE -->
    <title>Suez Techno Marine</title>

    <!-- FavIcon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicons/apple-touch-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/favicons/apple-touch-icon-60x60.png')}}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicons/favicon-32x32.png" sizes="32x32')}}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicons/favicon-16x16.png" sizes="16x16')}}">

    <!-- META-DATA -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- CSS:: FONTS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- CSS:: BOOTSTRAP -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">

    <!-- CSS:: ANIMATION -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}">


    <!-- CSS:: DATEPICKER -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css')}}">

    <!-- CSS:: OWL -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.css')}}">

    <!-- JQUERY:: BxSlider.CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.bxslider.css')}}">

    <!-- CSS:: -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css')}}">


<body>
<div class="y-home_02">

    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')

    <div class="y-back_to_top" id="y-back_to_top">
        <i class="fa fa-anchor"></i>
    </div>
    <div class="y-line"></div>
    <div class="y-loading" id="y-loading">
        <img src="{{ asset('assets/images/loading.gif')}}" alt="">
    </div>
</div>


<!-- JQUERY:: JQUERY.MIN.JS -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.min.js')}}"></script>
<!-- JQUERY:: WOW.JS -->
<script type="text/javascript" src="{{ asset('assets/js/wow.min.js')}}"></script>
<!-- JQUERY:: SKROLLER.JS -->
<script type="text/javascript" src="{{ asset('assets/js/skrollr.min.js')}}"></script>
<!-- JQUERY:: MENU.JS -->
<script type="text/javascript" src="{{ asset('assets/js/responsivemultimenu.js')}}"></script>
<!-- JQUERY:: BxSlider.JS -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.bxslider.min.js')}}"></script>
<!-- JQUERY:: OWL.JS -->
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
<!-- JQUERY:: DATEPICKER.JS -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.ui.js')}}"></script>
<!-- CUSTOM:: CUSTOM.JS -->
<script type="text/javascript" src="{{ asset('assets/js/custom.min.js')}}"></script>



</body>

</html>
