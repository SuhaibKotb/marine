<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@marine.app',
            'password' => 12345678
        ]);

        if (config('app.env') == 'production') {
            return;
        }

        Category::factory(10)->hasProducts(20)->create();
        Message::factory(20)->create();
    }
}
