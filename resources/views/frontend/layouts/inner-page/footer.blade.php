<footer id="y-footer">
    <div class="y-footer">
        <div class="container">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5><i class="material-icons">room</i> <span>Office I</span></h5>
                    <div class="y-contact_inner">
                        <address>Egypt, Ismailiyah, Ismailiyah the first , next to Gold's Gym</address>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5><i class="material-icons">room</i> <span>Office II</span></h5>
                    <div class="y-contact_inner">
                        <address>8 Gawhar El Qaed St., - Port Tawfik - Suez Next to: In Front Of Kingdom Of Saudi Arabia Consulate</address>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5><i class="material-icons">room</i> <span>Office III</span></h5>
                    <div class="y-contact_inner">
                        <address>El Gomhoureya St., Intersection Of De Lesseps St., El Shark District - Port Said – Egypt</address>
                    </div>
                </div>
{{--                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">--}}
{{--                    <h5><i class="material-icons">mood</i> <span>Special Offers</span></h5>--}}
{{--                    <div class="y-special_offer">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <a href="#">Special offer for you</a>--}}
{{--                                <p>We are constantly working to offer you the best value for money we can.</p>--}}
{{--                                <a href="#" class="y-read_more">Read More</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">--}}
{{--                    <h5><i class="material-icons">help</i> <span>Need Help?</span></h5>--}}
{{--                    <div class="y-need_help">--}}
{{--                        <ul>--}}
{{--                            <li><a href="#">Faq</a></li>--}}
{{--                            <li><a href="#">Payment options</a></li>--}}
{{--                            <li><a href="#">Privacy policy</a></li>--}}
{{--                            <li><a href="#">Terms and conditions</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="y-footer_strip">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <p class="text-left">
                            Copyright 2021 Suez Techno Marine Services, all rights reserved.
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 y-social_links">
                        <p>
                            <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
