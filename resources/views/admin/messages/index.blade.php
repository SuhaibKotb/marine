@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1299.69px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Messages Table</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Messages Table</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Messages</h3>
                                <div class="card-tools">
                                    <ul class="pagination pagination-sm float-right">
                                        {{ $messages->links('vendor.pagination.default') }}
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                        <th>Show/Edit/Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($messages as $message)
                                        <tr>
                                            <td>{{ $message->id }}</td>
                                            <td>{{ Str::limit($message->name, $limit = 15, $end = '...') }}</td>
                                            <td>{{ Str::limit($message->email, $limit = 20, $end = '...') }}</td>
                                            <td>{{ Str::limit($message->subject, $limit = 20, $end = '...') }}</td>
                                            <td>{{ Str::limit($message->message, $limit = 50, $end = '...') }}</td>
                                            <td><a href="messages/{{ $message->id }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('admin.products.destroy', $message->id) }}" class="btn btn-danger"
                                                   data-toggle="modal" data-target="#deleteModal{{$message->id}}"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="deleteModal{{$message->id}}" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Delete Message</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                                <form action="{{ route('admin.messages.destroy', $message->id) }}" method="post">
                                                  <div class="modal-body">
                                                    @csrf
                                                    @method('DELETE')
                                                    <h5 class="text-center">Are you sure you want to delete this Message?</h5>
                                                  </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-danger">Yes, Delete Message</button>
                                                    </div>
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->

                            </div>
{
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->
    </div>

<script>
    $(document).on('click','.delete',function(){
         let id = $(this).attr('data-id');
         $('#id').val(id);
    });
</script>
@endsection
