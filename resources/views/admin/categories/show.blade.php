@extends('admin.layouts.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> Category Details </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.categories.index') }}">Categories</a></li>
              <li class="breadcrumb-item active">{{ $category->name }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-sm-6">
              <h3 class="d-inline-block d-sm-none">{{ $category->name }}</h3>
            </div>
            <div class="col-12 col-sm-6">
              <h3>{{ $category->name }}</h3>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card -->
      @if($products->count())
          <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Products</h3>
                                    <div class="card-tools">
                                        <ul class="pagination pagination-sm float-right">
                                            {{ $products->links('vendor.pagination.default') }}
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover text-nowrap">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Price</th>
                                            <th>Category</th>
                                            <th>Last Updated at</th>
                                            <th>Show/Edit/Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{ $product->id }}</td>
                                                <td>{{ Str::limit($product->title, $limit = 30, $end = '...') }}</td>
                                                <td>{{ Str::limit($product->description, $limit = 50, $end = '...') }}</td>
                                                <td>{{ $product->price }}</td>
                                                <td>{{ $product->category->name }}</td>
                                                <td>{{ $product->updated_at }}</td>
                                                <td><a href="products/{{ $product->id }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                                    <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-success"><i class="fas fa-edit"></i></a>
                                                    <a href="{{ route('admin.products.destroy', $product->id) }}" class="btn btn-danger"
                                                    data-toggle="modal" data-target="#deleteModal{{$product->id}}"><i class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <div class="modal fade" id="deleteModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                    <form action="{{ route('admin.products.destroy', $product->id) }}" method="post">
                                                      <div class="modal-body">
                                                        @csrf
                                                        @method('DELETE')
                                                        <h5 class="text-center">Are you sure you want to delete this Product?</h5>
                                                      </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-danger">Yes, Delete Product</button>
                                                        </div>
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </section>
          </section>
      @else
          <h2>No Products</h2>
      @endif

  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <script>
        $('#staticBackdrop').modal(options)

        $(document).on('click','.delete',function(){
             let id = $(this).attr('data-id');
             $('#id').val(id);
        });
    </script>
@endsection
