@extends('frontend.layouts.master')
@section('content')
    <div class="y-slider_form">
        <div id="y-banner">
            <div class="y-banner">
                <div>
                    <img src="assets/images/home_banner_02.jpg" class="img-responsive" alt="">
                </div>
            </div>
        </div>
        <div id="y-header_form">
            <div class="container">
                <div class="row y-header_form">
                    <h3>search spare parts</h3>
                    <div class="clearfix row">
                        <form type="get" action="{{ route('products.index') }}" class="y-form">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div>
                                    <i class="material-icons">inventory</i>
                                    <input type="text" name="search_query" placeholder="Product">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div>
                                    <i class="material-icons">category</i>
                                    <select class="custom-select" name="category">
                                        <option>Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12  col-sm-12  col-md-12  col-lg-12 ">
                                <button class="y-button"><i class="material-icons">send</i> SEARCH FOR YACHT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="y-our_services">
        <div class="container y-our_services">
            <div class="row y-section_inner clearfix">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="y-service_inner">
                        <img src="assets/images/icon_01.png" alt="">
                        <div class="y-servie_info">
                            <span>Sailing destinations</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="y-service_inner">
                        <img src="assets/images/icon_02.png" alt="">
                        <div class="y-servie_info">
                            <span>hire for special events</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="y-service_inner">
                        <img src="assets/images/icon_03.png" alt="">
                        <div class="y-servie_info">
                            <span>charter guide</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="y-service_inner">
                        <img src="assets/images/icon_04.png" alt="">
                        <div class="y-servie_info">
                            <span>our fleet</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <section id="y-book_today">
        <div class="y-book_today">
            <div class="container">
                <div class="row y-section_inner">
                    <div class="y-section_content clearfix">
                        <div class="row">
                            <div class="col-lg-12 clearfix">
                                <div class="y-breadcrum clearfix wow">
                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                                        <h1 class="y-heading">About us</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="y-corporate_block clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                                        <img src="assets/images/aboutus_main_img.jpg" class="img-responsive" alt="">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <p>
                                            Suez Techno-Marine Services L.L.C is established to supply the highest quality ships’ supplies, technical equipment, spare parts and specialist-support service to a worldwide clientele base.
                                            In-house experience and practical knowledge go hand-in-hand with the dynamic enthusiasm and aspirations of Suez Techno-Marine Services L.L.C’s forward-looking team.
                                            Based on customers’ requirements, Suez Techno-Marine Services L.L.C provides quality equipment and spare parts from the world’s most reliable manufacturers, specializing in a wide scope of supply including Engine Room, Bridge, Galley, Hydraulic and Deck Equipment. Locally, we also offer provisions, stores, and vessel hardening. However, our main activities are based on marine engine repairs, providing: new and reconditioned marine engines, OEM (Original Equipment Manufacturer) or replacement spare parts.
                                            Our aim is to provide the highest possible service that our customers deserve. We take pride in our commitment to respond quickly and competitively, to meet the needs of our clientele. Reliability, flexibility, professionalism and the personal touch comprise the foundation of the services and products we provide.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="y-client_says_section">
        <div class="y-client_says_section">
            <div id="y-client_says_text" class="container">
                <div class="row y-section_inner">
                    <h2 class="text-center">WHAT CLIENTS SAY</h2>
                    <div id="y-client_testimonial_carousel">
                        <div class="y-client_testimonials">
                            <p class="y-client_testimonial_text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.
                            </p>
                            <div class="y-client_testimonial_user">
                                <span>Bill Gates <em>(Web Design)</em></span>
                                <div class="y-client_testimonial_rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="y-client_testimonials">
                            <p class="y-client_testimonial_text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.
                            </p>
                            <div class="y-client_testimonial_user">
                                <span>Henry Ford <em>(CEO founder)</em></span>
                                <div class="y-client_testimonial_rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="y-client_testimonials">
                            <p class="y-client_testimonial_text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus.
                            </p>
                            <div class="y-client_testimonial_user">
                                <span>Coco Chanel <em>(Marketing)</em></span>
                                <div class="y-client_testimonial_rating">
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="y-boat_type">
        <div class="y-boat_type">
            <div class="container">
                <div class="row y-section_inner">
                    <div class="y-section_content_full clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <h2 class="text-center">Our Products</h2>
                                <div class="y-boat_carousel col-lg-12 clearfix" id="y-boat_carousel">
                                    @foreach($products as $product)
                                    <div class="item">
                                        <div style="height: 80%; width: 60%;">
                                            <img src="{{ $product->image_url }}" alt="" class="img-responsive">
                                        </div>

                                        <div class="y-boat_info clearfix">
                                            <div class="col-sm-12 col-xs-12">
                                                <p>{{ $product->title }}</p>
                                                <a href="{{ route('products.index', 'category=' . $product->category_id)}}"><i>{{ $product->category->name }}</i></a>
                                                <span>{{ $product->price }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
